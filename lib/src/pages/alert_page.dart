
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AlertPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pagina Alert'),
      ),
      body: Center(
        child: RaisedButton(
          child: Text('Mostrar Alert'),
          onPressed: () => _modal(context),
          textColor: Colors.white,
          color: Colors.amber,
          shape: StadiumBorder(),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_back_ios,color: Colors.deepOrange),
        onPressed: (){
          Navigator.pop(context);
        },
        backgroundColor: Colors.black,
      ),
    );
  }

  void _modal(BuildContext context){
    showDialog(
      context: context,

      barrierDismissible: true,
      builder: (context){
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25.0),
          ),
          title: Text('Primer Modal'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text('Este es el primer testo'),
              Divider(height: 1.0),
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Primo'),
              onPressed: () => Navigator.of(context).pop()
            ),
            FlatButton(
              child: Text('Secondo'),
              onPressed: () => Navigator.of(context).pop(),
            )
          ],
        );
      }
    );
  }
}
