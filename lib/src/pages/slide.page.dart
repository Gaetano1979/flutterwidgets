import 'package:flutter/material.dart';

class SlidePage extends StatefulWidget {
  @override
  _SlidePageState createState() => _SlidePageState();
}

class _SlidePageState extends State<SlidePage> {
  double _valor = 100.0;
  bool _bloccareImg = false;
  bool _switch = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('App Slide'),
      ),
      body: Center(
        child: Container(
          padding: EdgeInsets.only(top: 50.0),
            child: Column(
          children: <Widget>[_crearSlider(),_crearCheckBox(),_crearSwitch(),_crearImg(),],
        )),
      ),
    );
  }

  Widget _crearSwitch(){
    return SwitchListTile(
      value: _switch,
      title: Text('Cambia il Switch'),
      onChanged: (valor) {
        setState(() {
          _switch = valor;
        });
      }  ,
    );
  }

  Widget _crearSlider() {
    return Slider(
      activeColor: Colors.deepOrange,
      label: 'Dimensione img',
      divisions: 25,
      value: _valor,
      max: 400.0,
      min: 10.0,
      onChanged: (_bloccareImg) ? null : (valor) {
        setState(() {
          print(valor);
          _valor = valor;
        });
      },
    );
  }

  Widget _crearImg() {
    return
      Container(
//        padding: EdgeInsets.symmetric(vertical: 20.0,horizontal: 20.0),
        child: Center(
          child: Image(
            width: _valor,
//            fit: BoxFit.fill,
            image: NetworkImage('https://lh3.googleusercontent.com/WaAKQ6CIQNxanBTAAhIg2CRi4QQ2K3AOGusO_L9l30k4idbVOGcdofkWQSkl1Q70cU97AvSmayxZmqp3SX-KI7EalOQdQy-3SnTjrm-OOB1mGPMMnLZc8PcmWiq-YZBMtGijRbaeAf8FyqMA61yWeAaDsPbGBxktzf0n-Kmz90vvSdSzUGNbPfkCzlsBjHDCucazr_YZnoZzIlBkku3cUNPaJkCjUwZWi8SjX306f-yZvBiQFWRO00Fk0mD0dG7u13vDx5e2nuPxHpaipSCk5U3n0n6tqhnXi4lqNSRM3Ui2VSspUF55HLDimyFX_vZCqvz6QxJlkZoNR5quoGm6WQaFvXzCBVYzDm5hYqv5okSj7F-zTP289UdDzqB973jvG-VlCfnL9j27rFuXrhWCvfASO5h5K1pkTQf-4fZqoYT12mAyXIRTsBI8dHX3zxAaThgAJqJ4Laag3ckPCJtoxfSNcNGV_XJGmv5Yw9LkqDVwZJTwgro5ODxIxUcJHBZG7HMvjNxw2wH6_2BXQRvxfbxfUPhejVsGo3s16D4q44fB8zdIl-v9xl3rFF6sWMFT9pcLPmZRT0onWLXYCUCpJFd227xD1GfjNHfIc-uTfaMpVtAVgRPXZ4cYb54zEjRixtN-UIhPSrERM-UCKRnRJJjUczS7-8eDOHLNB1wrEV04fbDWOJizxRzY1C3v=w884-h663-no'),
          ),
        ),
      );
      Image(
    );
  }

  Widget _crearCheckBox() {
    bool _p = false;
    return CheckboxListTile(
      value: _bloccareImg,
      title: Text('Cambia dimensione Img'),
      checkColor: Colors.white,
      activeColor: Colors.deepOrangeAccent,
      selected: _p,
      onChanged: (valor){
        setState(() {
          _bloccareImg = valor;
          _p = valor;
        });
      },

    );
  }
}
