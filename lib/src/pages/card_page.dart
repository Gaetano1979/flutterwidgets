import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Card Page'),
      ),
      body: ListView(
        children: <Widget>[
          _cards(),
          SizedBox(height: 35.0),
          _cards2(),
        ],
        padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 10.0),
      ),
    );
  }

  Widget _cards() {
    return Card(
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text('Hola a todos'),
            leading: Icon(Icons.folder_open, color: Colors.deepOrange),
          ),
          Row(
            children: <Widget>[
              FlatButton(
                child: Text('Conferma'),
                onPressed: () {},
              ),
              FlatButton(
                child: Text('Cancella'),
                onPressed: () {},
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          )
        ],
      ),
      elevation: 10.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0)
      ),
    );
  }

  Widget _cards2() {
    return Card(
      child: Container(
        padding: EdgeInsets.all(20.0),
        child: FadeInImage(
          fadeInDuration: Duration(milliseconds: 200),
          height: 250.0,
          fit: BoxFit.fill,
          placeholder: AssetImage('assets/original.gif'),
          image: NetworkImage('https://lh3.googleusercontent.com/WpYAl1QN6-7i_jJ4oLDLBBNWtMYalotT2-S067jEUlQFDHtWhmM0BPFoG83ESWhkcnSegSiHY2-kbWRrzf-a-IJ7wEvX1VDu9LCJu1ZoOeKFfor1KPCu8YPdlmz-T1sitgcMwmi_TQR4_kuEHR_RdFWlMDz-VOcub28ux7xDG5BskT51GnIAZaanT81F7TU1o_hRO0bLUXYZVWAsWnvJJ1Gp17yTVgUXrvsamM37cyRQkTFwGb-xLrsFnOsxlAnm8snO6tLEgc88l9PnK47V79LWFho_I-qyYDpUOCq_vnsTR22xzx-mRSQsd7tVAUtPLhu5N9BScANHJmA2IemvqbPlGypOpnOXzWrXpHxa9l1oFIdxR-B3pVkRzJvYE638tEgRVHWF6jSeae9yo_QpMokkGsgAiBxtfZnR2MmsyTYPye-F-MiZGTWv_VNIwuqP8TttLuO3xVmrrWDM6OLaSU_6cum-y2F0V2lmcSxcKnzzTMnnxRKFBRleWzNKuEemQY0gJH09wCc1ednzDOaDjLr0tyi1W3pLzcXovuUHOMowEqHtWKtgZkW-IYG-ap4WpUjuwo3y_TbsvD7kapnWNefk9ntWjnGTpFxSiO76Sb1R2f5GlBrq6oQOx4GGEfwbfhOmoRpkOh9A7NHII9y78lnL46IdgQXk-W7FCt1OT8DhBjXhpEjBx7uEiNv4=w884-h663-no'),
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0)
        ),
      ),
      elevation: 20.5,
    );
  }
}
