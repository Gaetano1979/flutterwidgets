import 'package:flutter/material.dart';


class AvatarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Avatar Page'),
        actions: <Widget>[
          Container(
            child: CircleAvatar(

              radius: 28.0,
              child: Container(
                child: FadeInImage(
                  placeholder: AssetImage('assets/original.gif'),
                  image: NetworkImage('https://lh3.googleusercontent.com/lKARxZeNjh96oUy29oDw6Uu0-ARRgLTddEO3wArK4LX8Ps8in7uoZiKxbQ0rRUI8urcKcyuc_j4m93jBpRGLn_51oVDOmUzWPGe91na9r2-LJSaw0iVXHqcWg_nhTuLekdXw3EtrV82ggGTJYmBxUwoC7JBWzXcn3_6K2xjSnnmkX3fFJjwWQxtHCUnKXWERvr78dktoXPJPRq7p4axRe0gpYn_uB70A-LDVDmL76c8rg558bHuqTPFgCRmy2WWqWkjb7RnipHjlEtiZsu5eFS5bTDLY12PWcgGLPAAMAxOtmD8uOD8yGVICrIrU5_xPUGx3XTcAHISMn3CJe6DxPx3ThqTYUoZMGDNYZL9h58kJxzB9LwtbyJ8Ps0XzQGv4zAFwoz10lDsA9PzUk-C3vBXqrHQDihT_HcoRjIlGZ3On4YaZgnkD7mKxXzsvgqKDK9eFynVCuzQAu48YmE8mUZUlZ8WCqYpi3gSQ4hjla0C6YGfAGe9xoX2UkIHu3zswgKDCwyxxXHZEujrWjL3Rq6DnVtwa52YcS_3bcx-3SsXnP31Mv7V7hOJeO-wW34bgx31vKfJ3F4EKVlQ1GkV4n5yrLChLdOwJThRKJHLOwYXTkAsVlkN-2odf372oAet5nLyM4XWyhjsDV8FhNDvOszDrKSQvv7fkNHeBEm4TmMwWfSH0diDTpiux47cp=w498-h663-no'),
                ),
              ),
            ),
          ),
          Container(
            child: CircleAvatar(
              child: Text('GS',style: TextStyle(color: Colors.white)),

              backgroundColor: Colors.cyan,
            ),
            margin: EdgeInsets.only(right: 10.0),
          )
        ],
      ),
      body: Center(
        child: Container(
          padding: EdgeInsets.all(30.0),
          child: FadeInImage(
            placeholder: AssetImage('assets/original.gif'),
            image: NetworkImage('https://lh3.googleusercontent.com/lKARxZeNjh96oUy29oDw6Uu0-ARRgLTddEO3wArK4LX8Ps8in7uoZiKxbQ0rRUI8urcKcyuc_j4m93jBpRGLn_51oVDOmUzWPGe91na9r2-LJSaw0iVXHqcWg_nhTuLekdXw3EtrV82ggGTJYmBxUwoC7JBWzXcn3_6K2xjSnnmkX3fFJjwWQxtHCUnKXWERvr78dktoXPJPRq7p4axRe0gpYn_uB70A-LDVDmL76c8rg558bHuqTPFgCRmy2WWqWkjb7RnipHjlEtiZsu5eFS5bTDLY12PWcgGLPAAMAxOtmD8uOD8yGVICrIrU5_xPUGx3XTcAHISMn3CJe6DxPx3ThqTYUoZMGDNYZL9h58kJxzB9LwtbyJ8Ps0XzQGv4zAFwoz10lDsA9PzUk-C3vBXqrHQDihT_HcoRjIlGZ3On4YaZgnkD7mKxXzsvgqKDK9eFynVCuzQAu48YmE8mUZUlZ8WCqYpi3gSQ4hjla0C6YGfAGe9xoX2UkIHu3zswgKDCwyxxXHZEujrWjL3Rq6DnVtwa52YcS_3bcx-3SsXnP31Mv7V7hOJeO-wW34bgx31vKfJ3F4EKVlQ1GkV4n5yrLChLdOwJThRKJHLOwYXTkAsVlkN-2odf372oAet5nLyM4XWyhjsDV8FhNDvOszDrKSQvv7fkNHeBEm4TmMwWfSH0diDTpiux47cp=w498-h663-no'),
          ),
        ),
      ),
    );
  }
}
