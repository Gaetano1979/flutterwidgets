import 'dart:async';

import 'package:flutter/material.dart';

class ListViewPage extends StatefulWidget {
  @override
  _ListViewPageState createState() => _ListViewPageState();
}

class _ListViewPageState extends State<ListViewPage> {
  List<int> _lista = new List();
  static int _ultimo = 0;
  static bool _isLoading = false;
  ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _addItems();
    _scrollController.addListener(() =>
    {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent)
        {
//        _addItems()
          _crearData()
        }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('App List View')),
      body: Stack(
        children: <Widget>[
          _crearList(),
          _creaLoading(),
        ],
      ),
    );
  }

  _crearList() {
    return RefreshIndicator(
      onRefresh: _ottenerePagUno,
      child: ListView.builder(

        controller: _scrollController,
        itemCount: _lista.length,
        itemBuilder: (BuildContext context, int index) {
          final imagine = _lista[index];
          return FadeInImage(
            image: NetworkImage('https://picsum.photos/500/300/?image=$imagine'),
            placeholder: AssetImage('assets/original.gif'),
          );
        },
      ),
    );
  }

  Future<Null> _ottenerePagUno() async {
    final duration = new Duration(seconds: 3);
    new Timer(duration, _callback$);

    return Future.delayed(duration);

  }
  void _callback$(){
    _lista.clear();
    _ultimo++;
    _addItems();
  }



  void _addItems() {
    for (var i = 1; i < 10; i++) {
      _ultimo++;
      _lista.add(_ultimo);
      setState(() {});
    }
  }

//  simular una chiamata http
  Future _crearData() async {
    _isLoading = true;
    final duration = new Duration(seconds: 2);
    return new Timer(duration, callback);
  }

  void callback() {
    _isLoading = false;
    _scrollController.animateTo(
        _scrollController.position.pixels + 100, duration: Duration(seconds: 2),
        curve: Curves.fastOutSlowIn);
    _addItems();
  }

  Widget _creaLoading() {
    if (_isLoading) {
      print('chiama il loading');
      return CircularProgressIndicator(
        backgroundColor: Colors.deepOrange,

      );
    } else {
      return Container();
    }
  }


}
