import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttercomponentes/src/pages/alert_page.dart';
import 'package:fluttercomponentes/src/provaiders/menu_providers.dart';
import 'package:fluttercomponentes/src/util/util_icon.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Page'),
      ),
      body: _lista(),
    );
  }

  Widget _lista() {
    // Todo quando aspettiamo dati async meglio usare un futureBuilder
    return FutureBuilder(
      future: menuProvider.cargarData(),
      initialData: [],
      builder: (context, AsyncSnapshot<List<dynamic>> snapshopt) {
        print('metodo');
        print(snapshopt.data);
        return ListView(
          children: _listaItems(snapshopt.data,context),
        );
      },
    );
//    print(menuProvider.opciones);
//  menuProvider.cargarData().then((lista){
//    print(lista);
//
//  });
//    return ListView(
//      children: _listaItems(),
//    );
  }

  List<Widget> _listaItems(List<dynamic> data, BuildContext context) {
    return data.map((item){
      return Column(
        children: <Widget>[
          ListTile(
            title: Text(item['texto']),
            leading: getIcon(item['icon'], 'nero'),
            trailing: Icon(Icons.arrow_forward_ios,color: Colors.blue,),
            onTap: (){
              Navigator.pushNamed(context, item['ruta']);
//              print('testo');
//              final route = MaterialPageRoute(
//                builder: (context){
//                  return AlertPage();
//                }
//              );
//              Navigator.push(context, route);
            },
          ),
          Divider(),
        ],
      );
    }).toList();
    return [
      ListTile(title: Text('Hola a todos')),
      Divider(),
      ListTile(title: Text('Hola a todos')),
      Divider(),
      ListTile(title: Text('Hola a todos')),
      Divider(),
      ListTile(title: Text('Hola a todos')),
      Divider(),
      ListTile(title: Text('Hola a todos')),
    ];
  }
}
