import 'dart:math';

import 'package:flutter/material.dart';


class ContainerClass extends StatefulWidget {
  @override
  _ContainerClassState createState() => _ContainerClassState();
}

class _ContainerClassState extends State<ContainerClass> {
  static double _width = 150.0;
  static double _height = 100.0;
  static Color _color = Colors.amber;
  static BorderRadiusGeometry radius = BorderRadius.circular(9.0);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Container App'),
      ),
      body: Center(
        child: AnimatedContainer(
          duration: Duration(
            milliseconds: 500
          ),
          curve: Curves.bounceIn,
          width: _width,
          height: _height,
          child: Center(child: Text('Container Center')),
          decoration: BoxDecoration(
            color: _color,
            borderRadius: BorderRadius.circular(30.0)
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.play_circle_filled),
        onPressed: _cambiarForma,
      ),

    );
  }

  void _cambiarForma(){

    final random = Random();

    setState(() {
      _width  = random.nextInt(300).toDouble();
      _height  = random.nextInt(300).toDouble();
      _color = Color.fromRGBO(
          random.nextInt(255), random.nextInt(255), random.nextInt(255), 1
      );
      radius = BorderRadius.circular(random.nextInt(10).toDouble());
    });
  }
}

