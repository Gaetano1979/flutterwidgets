import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  static String _nome = '';
  static String _email = '';
  static String _pass = null;
  static String _fecha = null;
  static bool _switch = false;
  String _optOption = 'Volare';

  static List<String> _listaPoderes = ['Volare','Raggio X','Super Forza','Super Velocitá'];

  TextEditingController _inputFechaController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('App Input'),
      ),
      body: Center(
        child: Container(
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
            children: <Widget>[
              _crearInput(),
              Divider(),
              _crearPersona(),
              Divider(),
              _crearEmail(),
              Divider(),
              _vederEmail(),
              Divider(),
              _crearPassword(),
              Divider(),
              _creaFecha(context),
              Divider(),
              _crearDropDown(),Divider(),
//              _crearSwitch()
            ],
          ),
        ),
      ),
    );
  }

  TextField _crearInput() {
    final primo = TextField(
      textCapitalization: TextCapitalization.sentences,
      onChanged: (valor) {
        setState(() {
          _nome = valor;
        });
      },
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          labelText: "Nome",
          counter: Text('Letras ${_nome.length}'),
          helperText: 'Solo nome',
          suffixIcon: Icon(Icons.check),
          icon: Icon(Icons.accessibility)),
    );
    return primo;
  }

  Widget _crearPersona() {
    return ListTile(
      title: Text('Mi nombre es: $_nome'),
    );
    setState(() {});
  }

  Widget _crearEmail() {
    final email = TextField(
      keyboardType: TextInputType.emailAddress,
      onChanged: (valor) {
        setState(() {
          _email = valor;
        });
      },
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          labelText: "Email",
          counter: Text('Email ${_email.length}'),
          helperText: "Ingressare Email",
          suffixIcon: Icon(Icons.alternate_email),
          icon: Icon(Icons.email)),
    );
    return email;
  }

  Widget _vederEmail() {
    return ListTile(
      title: Text('La sua email es: $_email'),
    );
  }

  TextField _crearPassword() {
    final password = TextField(
      keyboardType: TextInputType.visiblePassword,
      obscureText: true,
      onChanged: (pass) => setState(() => {_pass = pass}),
      decoration: InputDecoration(
        labelText: "Password",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        icon: Icon(Icons.lock),
        suffixIcon: Icon(Icons.lock_open),
        helperText: "Ingressare Password",
        counter: Text('password'),
      ),
    );
    return password;
  }

  Widget _creaFecha(BuildContext context){
    return TextField(
      controller: _inputFechaController,
      enableInteractiveSelection: false,
      decoration: InputDecoration(
        labelText: "Data"
      ),
      onTap: (){
        FocusScope.of(context).requestFocus(new FocusNode());
        _selectDate(context);
      },
    );
  }

  void _selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(context: context,
        initialDate: new DateTime.now(), firstDate: new DateTime(2015), lastDate: new DateTime(2025),locale: Locale('it','IT'));
    if (picked != null) {
      _fecha = picked.toString();
      _inputFechaController.text = _fecha;
    }
  }

  List <DropdownMenuItem<String>> getOptionDrop() {
    List<DropdownMenuItem<String>> lista = new List();

    _listaPoderes.forEach((poder){
      lista.add(DropdownMenuItem(
        child: Text(poder),
        value: poder,
      ));
    });
    return lista;
}

  Widget _crearDropDown() {
    return DropdownButton(
      value: _optOption,
      isExpanded: true,

      items: getOptionDrop(),
      onChanged: (String opt){
        setState(() {
          print(opt);
          _optOption = opt;
        });
      },
    );
  }


}
