import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {
  final lista = ['Uno', 'Due', 'Tre', 'Quattro', 'Cinque', 'Sei'];
  final contador = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes App Componentes'),
      ),
      body: ListView(
        children: _creaList(),
      ),
    );
  }

  // Todo Possiamo creare lista di forma diversa
  List<Widget> _creaList() {
    return lista.map((item) {
      return Column(
        children: <Widget>[
          ListTile(
            title: Text(item),
            subtitle: Text(item + ' $contador'),
            leading: Icon(
              Icons.accessibility
            ),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: (){},
          ),
          Divider(),
        ],
      );
    }).toList();
  }
}
