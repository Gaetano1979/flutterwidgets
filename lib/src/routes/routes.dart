import 'package:flutter/material.dart';
import 'package:fluttercomponentes/src/pages/alert_page.dart';
import 'package:fluttercomponentes/src/pages/animate_container.dart';
import 'package:fluttercomponentes/src/pages/avatar_page.dart';
import 'package:fluttercomponentes/src/pages/card_page.dart';
import 'package:fluttercomponentes/src/pages/home_page.dart';
import 'package:fluttercomponentes/src/pages/input.page.dart';
import 'package:fluttercomponentes/src/pages/listView.page.dart';
import 'package:fluttercomponentes/src/pages/slide.page.dart';

Map<String, WidgetBuilder> getRoutes() {
  return <String, WidgetBuilder>{
    'home': (BuildContext context) => HomePage(),
    'alert': (BuildContext context) => AlertPage(),
    'avatar': (BuildContext context) => AvatarPage(),
    'card': (BuildContext context) => CardPage(),
    'container':(BuildContext context) => ContainerClass(),
    'input':(BuildContext context) => InputPage(),
    'slide':(BuildContext context) => SlidePage(),
    'lista':(BuildContext context) => ListViewPage(),

  };
}
