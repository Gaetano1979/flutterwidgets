import 'dart:convert';

import 'package:flutter/services.dart' show rootBundle;


class _MenuProviders {
  List<dynamic> opciones = [];

  _MenuProviders() {
    cargarData();
  }

  Future<List<dynamic>> cargarData() async {
    final resp = await rootBundle.loadString('data/menu_opts.json');

    Map dataJson = json.decode(resp);
//    print(resp);
    return opciones = dataJson['rutas'];
  }
}

final menuProvider = new _MenuProviders();