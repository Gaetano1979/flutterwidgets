import 'package:flutter/material.dart';

final _icon = <String, IconData>{
  'add_alert': Icons.add_alert,
  'accessibility': Icons.accessibility,
  'folder_open': Icons.folder_open,
  'all_out': Icons.all_out,
  'input':Icons.input,
  'slide':Icons.slideshow,
  'lista':Icons.format_list_bulleted
};

final _colores = <String, Color>{
  'nero': Colors.black,
  'blue': Colors.blue,
  'giallo': Colors.yellow,
};

Icon getIcon(String nomeIcon, String colore) {
  return Icon(_icon[nomeIcon], color: _colores[colore]);
}
