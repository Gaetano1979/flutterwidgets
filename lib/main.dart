import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:fluttercomponentes/src/pages/alert_page.dart';
import 'package:fluttercomponentes/src/routes/routes.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: 'App Componentes',
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      supportedLocales: [
        const Locale('it','IT'),
        const Locale('es','ES')
      ],
      debugShowCheckedModeBanner: false,
      initialRoute: 'home',
      routes: getRoutes(),
      onGenerateRoute: (RouteSettings setting) {
        print('Ruta ${setting.name}');
        return MaterialPageRoute(builder: (BuildContext context) {
          return AlertPage();
        });
      },

//      home: HomePage(),
    );
  }
}
